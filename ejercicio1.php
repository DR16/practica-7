<?php
Class areaFigura{
//aca se declaran los atributos de la clase
	protected $ancho;
	protected $largo;
	protected $resultado;

	//atraves del contructor de la clase
	//se utiliza para darle un valor a los atributos del objeto al crearlo.
	function __construct($v_largo, $v_ancho){
		$this->ancho=$v_ancho;
		$this->largo=$v_largo;
}
	//con esta funcion destructse encargan de realizar las tareas que se necesita ejecutar cuando un objeto deja de existir. Cuando un objeto ya no está referenciado por ninguna variable, deja de tener sentido que esté almacenado en la memoria, por tanto, el objeto se debe destruir para liberar su espacio.
	function __destruct(){
		echo "<br>objeto tipo areaFigura destruido.<br>";
	}
 		//con esta funcion se hace la operacion de 
		//calcular, la cual atraves de las variables de la clase.
	function calcularArea(){
		$this->resultado=$this->ancho * $this->largo;
		return $this->resultado;
	}
}
//aca se hereda de la clase base hacia la clase derivada
Class volumenFigura extends areaFigura{
	private $alto;
	private $volumen;
	private $var_ancho;
	private $var_largo;
   // se utiliza parent:: porque 
   // no son llamados implícitamente si la clase  define un constructor. 
   function __construct($v_alto, $v_largo, $v_ancho){
   		parent ::__construct($v_alto,$v_largo);
   		$this->alto=$v_ancho;
   }
//aca se destruira el objeto para que no se ocupe espacio en memoria
   function __destruct (){
   	echo "<br>Objeto volumenFigura destruido.<br>";
   }
   //se calcular atraves de la funcion el volumen de la figura
   function calcularVolumen(){
   	$this->volumen=($this->ancho)* ($this->largo)*($this->alto);
   	return $this->volumen;
   }

}

?>